<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 2020-01-21
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Add Minutes</title>
    <script type="text/javascript" src="../jquery.js" > </script>
    <script type="text/javascript">
        $(document).ready(
            function () {
                $.getJSON('<spr:url value="/activities.json"/>', {
                    ajax: 'true'
                }, function (data) {
                   var html = '<option value=""> -- Please select one -- </option>';
                   var len = data.length;
                   for(var i=0; i<len; i++) {
                       html += '<option value="' + data[i].description +'">'+data[i].description+'</option>';
                   }
                   $('#activities').html(html)
                });
            });

    </script>
</head>
<body>
<h1>Add Minutes Exercised</h1>
Language: <a href="?language=en">english</a> | <a href="?language=el">ελληνικά</a>
<f:form modelAttribute="exercise" >
    <table>
        <tr>
            <td><spr:message code="goal.text" /></td>
            <td><f:input path="minutes" /></td>
            <td><f:select id="activities" path="activity" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="submit" value="Enter Exercise" />
            </td>
        </tr>
    </table>
</f:form>

<h1>OUr goal is ${goal.minutes}</h1>

</body>
</html>
