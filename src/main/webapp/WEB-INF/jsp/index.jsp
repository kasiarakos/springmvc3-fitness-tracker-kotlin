<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 16/03/2020
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Home page</title>
</head>
<body>
    <h1>Welcome to fitness tracker <sec:authentication property="name" />!</h1>
    <sec:authorize access="hasRole('ROLE_ADMIN')" >
        <button>click</button>
    </sec:authorize>
</body>
</html>
