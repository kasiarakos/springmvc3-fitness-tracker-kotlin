<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 18/02/2020
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Goal report</title>
</head>
<body>
    <table>
        <tr>
            <th>Goal Minutes</th>
            <th>Exercise Minutes</th>
            <th>Activity</th>
        </tr>
        <c:forEach items="${goalReports}" var="goalReport">
            <tr>
                <td>${goalReport.goalMinutes}</td>
                <td>${goalReport.exerciseMinutes}</td>
                <td>${goalReport.activity}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
