<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 17/03/2020
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
    <style type="text/css">
        .errorblock {
            color: #ff0000;
            background-color: #FFEEEE;
            border: 3px solid #FF0000;
            padding: 8px;
            margin: 16px;
        }
    </style>
</head>
<body onload="document.f.j_username.focus();">
    <h3>Fitness Tracker Custom Login Page</h3>
    <c:if test="${not empty error}">
        <div class="errorblock">
            Your login was unsuccessful. <br/>
            Caused by ${ sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message }
        </div>
    </c:if>

    <form action="login" name="f" method="post">
        <table>
            <tr>
                <td>User</td>
                <td><input type="text" name="username" value=""></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" name="submit" value="Submit" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="reset" name="reset" value="Reset"/></td>
            </tr>
        </table>
    </form>
</body>
</html>
