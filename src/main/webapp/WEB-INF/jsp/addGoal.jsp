<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 2020-01-22
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Add goal</title>
    <style>
        .error{
            color: #FF0000;
        }
        .errorblock {
            color: #000000;
            background-color: #FFEEEE;
            border: 3px solid #FF0000;
            padding: 8px;
            margin: 16px;
        }
    </style>
</head>
<body>
<spr:url var = "logoutAction" value='/logout'  />
<f:form modelAttribute="goal">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table>
        <tr>
            <td>Enter Minutes</td>
            <td><f:input path="minutes" cssErrorClass="error" /><td>
            <td><f:errors path="minutes" cssClass="error" /><td>
        </tr>
        <tr>
            <td colspan="3"><input type="submit" value="Enter Goal Minutes"/></td>
        </tr>
        <tr>
            <td colspan="3"><a href="${logoutAction}"  >logout</a></td>
        </tr>
    </table>
</f:form>



</body>
</html>
