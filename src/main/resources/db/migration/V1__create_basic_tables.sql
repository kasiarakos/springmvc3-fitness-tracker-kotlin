create table  Activity (
   id bigint not null,
    description varchar(255),
    primary key (id)
) engine=InnoDB;

create table Exercise (
    id bigint not null,
    activity varchar(255),
    minutes integer,
    goal_id bigint,
    primary key (id)
) engine=InnoDB;

create table Goal (
   id bigint not null,
    minutes integer not null,
    primary key (id)
) engine=InnoDB;

create table hibernate_sequence (
   next_val bigint
) engine=InnoDB;

insert into hibernate_sequence values ( 1 );

alter table Exercise
   add constraint FK62g0pbgi4dj58bwajqdg21yok
   foreign key (goal_id)
   references Goal (id);