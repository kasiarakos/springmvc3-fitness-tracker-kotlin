create table permissions (
username varchar(50) not null ,
target varchar (50) not null ,
permission varchar (50) not null,
constraint  fk_permissions_users foreign  key (username) references users(username)
);

create unique index ix_perm_username on permissions (username, target, permission);

insert into permissions (username, target, permission)
values ('kasiarakos', 'com.kasiarakos.domain.Goal' , 'createGoal')