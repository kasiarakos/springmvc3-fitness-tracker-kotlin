create table  users (
   username varchar(50) not null,
    password varchar(150),
    enabled boolean default true,
    primary key (username)
) engine=InnoDB;

create table  authorities (
   username varchar(50) not null,
    authority varchar(50),
    primary key (username)
) engine=InnoDB;

alter table authorities
   add constraint fk_authorities_users
   foreign key (username)
   references users (username);

create unique index indx_auth_username on authorities(username, authority);