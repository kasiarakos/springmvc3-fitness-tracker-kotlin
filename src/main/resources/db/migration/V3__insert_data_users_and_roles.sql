insert into users (username, password, enabled) values ("kasiarakos", '$2a$10$vgz0z/QUU1SMv3pzpZKJkONMaiq9/zRJ/iNAfPPY3RAiV27SlXkfy', true);
insert into users (username, password, enabled) values ("sofia", '$2a$10$vgz0z/QUU1SMv3pzpZKJkONMaiq9/zRJ/iNAfPPY3RAiV27SlXkfy', true);
insert into authorities (username, authority) values ("kasiarakos", "ROLE_USER");
insert into authorities (username, authority) values ("sofia" , "ROLE_BAD");