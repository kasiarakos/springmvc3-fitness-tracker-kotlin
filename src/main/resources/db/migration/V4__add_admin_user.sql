insert into users (username, password, enabled) values ("nefeli", '$2a$10$vgz0z/QUU1SMv3pzpZKJkONMaiq9/zRJ/iNAfPPY3RAiV27SlXkfy', true);
insert into authorities (username, authority) values ("nefeli", "ROLE_ADMIN");
