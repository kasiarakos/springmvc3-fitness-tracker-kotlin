package com.kasiarakos.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.GetMapping

@Controller
class LoginController {

    @GetMapping("/login")
    fun login(model: ModelMap): String {
        return "login"
    }

    @GetMapping("/login-failed")
    fun loginFailed(model: ModelMap): String {
        model.addAttribute("error", true)
        return "login"
    }

    @GetMapping("logout-success")
    fun logout(): String {
        return "logout"
    }

    @GetMapping("/403")
    fun pageNotFound() : String{
        return "403";
    }
}