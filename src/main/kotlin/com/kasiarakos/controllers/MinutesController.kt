package com.kasiarakos.controllers

import com.kasiarakos.domain.Activity
import com.kasiarakos.domain.Exercise
import com.kasiarakos.domain.Goal
import com.kasiarakos.services.ExerciseService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpSession

@Controller
class MinutesController(val exerciseService: ExerciseService) {


    @GetMapping("/addMinutes")
    fun  addMinutes(@ModelAttribute("exercise") exercise: Exercise ) : String {
        println("exercise $exercise")
        return "addMinutes"
    }

    @PostMapping("/addMinutes")
    fun addMinutes(@ModelAttribute("exercise") exercise: Exercise, session: HttpSession) : String {

        val goal = session.getAttribute("goal") as Goal
        exercise.goal = goal
        exerciseService.save(exercise);

        return "addMinutes"
    }

    @GetMapping("/activities" )
    @ResponseBody fun findAllActivities()  : List<Activity> {
        return listOf(Activity(description="running"),Activity(description="swimming"),Activity( description="rowing"))
    }

}