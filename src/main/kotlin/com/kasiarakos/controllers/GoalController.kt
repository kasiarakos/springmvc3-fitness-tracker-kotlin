package com.kasiarakos.controllers

import com.kasiarakos.aop.SimpleService
import com.kasiarakos.domain.Goal
import com.kasiarakos.services.GoalService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpSession
import javax.validation.Valid


@Controller
@SessionAttributes("goal")
open class GoalController(private val goalService: GoalService, private val simpleService: SimpleService) {


    @GetMapping("/addGoal")
    open fun addGoal(model: Model, httpSession: HttpSession): String {
        simpleService.doSomething()
        var goal = httpSession.getAttribute("goal") as Goal? ?: Goal(minutes = 10)
        model.addAttribute("goal", goal)
        return "addGoal"
    }

    @PostMapping("/addGoal")
    @PreAuthorize("hasRole('ROLE_USER') and hasPermission(#goal, 'createGoal')")
    open fun addGoalPost(@Valid @ModelAttribute("goal") goal: Goal, result: BindingResult): String {
        println(goal)

        println("Result has errors: ${result.hasErrors()}")
        if (result.hasErrors()) {
            return "addGoal"
        }

        goalService.save(goal)
        return "redirect:/addMinutes"
    }

    @GetMapping("/getGoals")
    open fun getGoals(model: Model): String {
        val goals = goalService.getGoals()
        model.addAttribute("goals", goals)
        return "getGoals"
    }

    @GetMapping("/getGoalReports")
    open fun getGoalReports(model: Model): String {
        val goalReports = goalService.getGoalReports()
        model.addAttribute("goalReports", goalReports)
        return "getGoalReports"
    }
}