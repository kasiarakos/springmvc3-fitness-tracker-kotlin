package com.kasiarakos.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HelloController {

    @GetMapping("/greeting")
    fun sayHello(model: Model) : String{

        model.addAttribute("greeting" , "Hello Kasiarakos")

        return "hello"
    }
}