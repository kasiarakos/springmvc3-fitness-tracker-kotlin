package com.kasiarakos.security

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.security.access.PermissionEvaluator
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import java.io.Serializable
import javax.sql.DataSource
import kotlin.reflect.jvm.jvmName

class FitnessPermissionEvaluator(private var dataSource: DataSource) : PermissionEvaluator {


    override fun hasPermission(auth: Authentication?, targetObject: Any?, permission: Any?): Boolean {
        val jdbcTemplate = JdbcTemplate(dataSource)
        val args = arrayOf(auth?.name, targetObject!!::class.jvmName,permission.toString())
        val count = jdbcTemplate.queryForObject("select count(*) from permissions p where p.username = ? and p.target = ? and p.permission = ?", args, Int::class.java)
        return count == 1
    }

    override fun hasPermission(p0: Authentication?, p1: Serializable?, p2: String?, p3: Any?): Boolean {
        TODO("Not yet implemented")
    }
}