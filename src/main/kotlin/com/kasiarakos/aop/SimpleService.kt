package com.kasiarakos.aop

import com.kasiarakos.aop.annotations.Trace
import org.springframework.stereotype.Service

@Service
open class SimpleService {
    open fun doSomething() {}

    @Trace
    open fun doSomethingElse() {}
}