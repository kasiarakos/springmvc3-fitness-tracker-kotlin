package com.kasiarakos.aop

import org.springframework.stereotype.Repository

@Repository
open class SimpleRepository {
    open fun doSomething() {
        println("Repo do something")
    }
}