package com.kasiarakos.aop

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.After
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.stereotype.Component

@Aspect
@Component
class TracingAspect {

    @Before("execution(void doSomething())")
    fun entering(joinPoint: JoinPoint) {
        println("entering at ${joinPoint.staticPart.signature}")
    }

    @After("execution(@com.kasiarakos.aop.annotations.Trace * *(..))")
    fun enteringAnnotation() {
        println("yolo");
    }

    @Around("bean(simpleRepository)")
    fun enteringAnnotation(proceedingJoinPoint: ProceedingJoinPoint) {
        println("yolo before")
        proceedingJoinPoint.proceed()
        println("yolo after")
    }
}