package com.kasiarakos.domain

import javax.persistence.*

@Entity
data class Exercise(

        @Id
        @GeneratedValue
        var id: Long?,

        var minutes: Int? = 0,

        var activity: String? = null,

        @ManyToOne
        var goal : Goal?
)