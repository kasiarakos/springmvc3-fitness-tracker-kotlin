package com.kasiarakos.domain

import com.kasiarakos.domain.Goal.Companion.FIND_ALL_GOALs
import com.kasiarakos.domain.Goal.Companion.FIND_GOAL_REPORTS

import javax.persistence.*

@Entity
@NamedQueries(
        NamedQuery(name = FIND_GOAL_REPORTS , query = "select new com.kasiarakos.dto.GoalReport(g.minutes, e.minutes, e.activity) from Goal g, Exercise e where g.id = e.goal.id"),
        NamedQuery(name = FIND_ALL_GOALs , query = "select g from Goal g")
)
data class Goal(

        @field:Id
        @field:GeneratedValue
        var id: Long? = null,

        var minutes: Int,

        @OneToMany(mappedBy = "goal", cascade = [CascadeType.ALL])
        val exercises: MutableList<Exercise> = mutableListOf()

        ){

        companion object {

                const val FIND_GOAL_REPORTS = "findGoalReports"
                const val FIND_ALL_GOALs = "findAllGoals"
        }
}