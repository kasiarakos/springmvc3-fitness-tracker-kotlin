package com.kasiarakos

import org.apache.catalina.LifecycleException
import org.apache.catalina.startup.Tomcat
import org.apache.tomcat.util.scan.StandardJarScanner

object Main {
    @Throws(LifecycleException::class)
    @JvmStatic
    fun main(args: Array<String>) {

        val tomcat = Tomcat()
        val baseDir = "."
        tomcat.apply {
            setPort(9090)
            setBaseDir(baseDir)
            host.appBase = baseDir
            host.autoDeploy = true
            enableNaming()
        }

        val context = tomcat.addWebapp(tomcat.host, "/fitness-tracker", "src/main/webapp")
        val scanner = StandardJarScanner()

        scanner.apply {
            isScanClassPath = true
            isScanManifest = false
        }

        context.jarScanner = scanner
        tomcat.connector

        tomcat.start()
        tomcat.server.await()
    }
}