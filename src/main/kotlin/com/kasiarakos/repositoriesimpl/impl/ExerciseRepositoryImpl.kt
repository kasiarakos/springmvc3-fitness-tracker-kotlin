package com.kasiarakos.repositoriesimpl.impl

import com.kasiarakos.domain.Exercise
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class ExerciseRepositoryImpl {

    @PersistenceContext
    lateinit var em: EntityManager

    fun save(exercise: Exercise): Exercise {
        em.persist(exercise)
        em.flush()
        return exercise
    }
}