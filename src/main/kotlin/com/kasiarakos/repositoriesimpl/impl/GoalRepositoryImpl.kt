package com.kasiarakos.repositoriesimpl.impl

import com.kasiarakos.domain.Goal
import com.kasiarakos.dto.GoalReport
import com.kasiarakos.repositories.GoalRepository
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class GoalRepositoryImpl : GoalRepository {

    @PersistenceContext
    private lateinit var  em: EntityManager

    override fun save(goal: Goal): Goal {

        if (goal.id == null) {
            em.persist(goal)
            em.flush()
        } else {
            return em.merge(goal)
        }
        return goal
    }

    override fun findAll(): List<Goal> {
        /*val query: Query = em.createQuery("select g from Goal g")*/
        return em.createNamedQuery(Goal.FIND_ALL_GOALs, Goal::class.java).resultList
    }

    override fun findGoalReports(): List<GoalReport> {
        /*val query = em.createQuery("select new com.kasiarakos.dto.GoalReport(g.minutes, e.minutes, e.activity) " +
                "from Goal g, Exercise e where g.id = e.goal.id")*/
        val query =  em.createNamedQuery(Goal.FIND_GOAL_REPORTS, GoalReport::class.java)
        return query.resultList
    }
}