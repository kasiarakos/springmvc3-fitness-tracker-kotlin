package com.kasiarakos.services.impl

import com.kasiarakos.domain.Activity
import com.kasiarakos.domain.Exercise
import com.kasiarakos.repositories.ExerciseRepository
import com.kasiarakos.services.ExerciseService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service

open class ExerciseServiceImpl(val exerciseRepository: ExerciseRepository) : ExerciseService {
    override fun getActivities(): List<Activity> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Transactional
    override fun save(exercise: Exercise): Exercise = exerciseRepository.save(exercise)

}