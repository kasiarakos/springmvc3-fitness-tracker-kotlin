package com.kasiarakos.services.impl

import com.kasiarakos.domain.Goal
import com.kasiarakos.dto.GoalReport
import com.kasiarakos.repositories.GoalRepository
import com.kasiarakos.services.GoalService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
open class GoalServiceImpl(private var goalRepository: GoalRepository) : GoalService {

   override fun save(goal: Goal) : Goal = goalRepository.save(goal)
   override fun getGoals(): List<Goal> = goalRepository.findAll()
   override fun getGoalReports(): List<GoalReport> = goalRepository.findGoalReports()

}