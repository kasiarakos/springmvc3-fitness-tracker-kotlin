package com.kasiarakos.services

import com.kasiarakos.domain.Activity
import com.kasiarakos.domain.Exercise

interface ExerciseService {
    fun getActivities(): List<Activity>
    fun save(exercise: Exercise) : Exercise
}