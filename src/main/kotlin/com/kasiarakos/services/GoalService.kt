package com.kasiarakos.services

import com.kasiarakos.domain.Goal
import com.kasiarakos.dto.GoalReport

interface GoalService {
    fun save(goal: Goal) : Goal
    fun getGoals() : List<Goal>
    fun getGoalReports(): List<GoalReport>
}