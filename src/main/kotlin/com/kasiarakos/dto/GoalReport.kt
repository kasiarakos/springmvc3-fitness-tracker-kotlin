package com.kasiarakos.dto


data class GoalReport(val goalMinutes: Int, val exerciseMinutes: Int,  val activity: String)