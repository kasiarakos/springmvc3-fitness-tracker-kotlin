package com.kasiarakos.repositories

import com.kasiarakos.domain.Goal
import com.kasiarakos.dto.GoalReport
import org.springframework.data.repository.RepositoryDefinition

@RepositoryDefinition(domainClass = Goal::class, idClass = Long::class)
interface GoalRepository {
    fun save(goal: Goal): Goal
    fun findAll(): List<Goal>

    fun findGoalReports() : List<GoalReport> //this should be the same with the namedQuery name
   /* @Query("select new com.kasiarakos.dto.GoalReport(g.minutes, e.minutes, e.activity) from Goal g, Exercise e where g.id = e.goal.id")
    fun getGoalReports(): List<GoalReport>*/
}