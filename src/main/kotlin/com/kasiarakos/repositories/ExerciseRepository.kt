package com.kasiarakos.repositories

import com.kasiarakos.domain.Exercise
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.RepositoryDefinition


interface ExerciseRepository : JpaRepository<Exercise, Long>{

    fun save(exercise: Exercise) : Exercise
}