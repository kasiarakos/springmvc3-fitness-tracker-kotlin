import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.kasiarakos.aop.SimpleRepository;
import com.kasiarakos.aop.TracingAspect;
import com.kasiarakos.aop.SimpleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aopContext.xml")
public class SimpleAspectJXML {

    @Autowired
    TracingAspect tracingAspect;

    @Autowired
    SimpleService simpleService;

    @Autowired
    SimpleRepository simpleRepository;

    @Test
    public void aspectIsCalled() {
        simpleService.doSomething();
    }

    @Test
    public void aspectAnnotationIsCalled() {
        simpleService.doSomethingElse();
    }

    @Test
    public void aspectBeanIsCalled() {
        simpleRepository.doSomething();
    }

    @Test
    public void testBCrypt() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        System.out.println(passwordEncoder.encode("1234"));
    }
}

